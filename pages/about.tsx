import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";

import styles from "../styles/Home.module.css";

const About: NextPage = () => {
  return (
    <div className={styles.main}>
      <Head>
        <title>About us</title>
        <meta
          name="description"
          content="All details we need to know from us"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <h1 className={styles.title}>About page - Im routing</h1>
        <p className={styles.description}>
          <span className={styles.link}>
            <Link href="/contact">
              <a>Back to contact</a>
            </Link>
          </span>
        </p>
      </main>
    </div>
  );
};

export default About;
