import Head from "next/head";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function ContactUs() {
  return (
    <>
      <Head>
        <title>Contact us</title>
        <meta
          name="description"
          content="Do you want to get in touch we us ? Let gimp right in"
        />
      </Head>
      <main className={styles.main}>
        <h1 className={styles.title}>Our contact</h1>
        <p className={styles.description}>
          <span className={styles.link}>
            <Link href="/about">
              <a>Back to about</a>
            </Link>
          </span>
          <span className={styles.link}>
            <Link href="/">
              <a>Back to home</a>
            </Link>
          </span>
        </p>
      </main>
    </>
  );
}
