This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Usefull resources

- apple-touch-icon
- [Service-worker](https://github.com/hanford/next-offline/blob/master/readme.md)
- [\_document.tsx](https://daily-dev-tips.com/posts/nextjs-add-lang-attribute-to-html-tag/)
- [Les balises a surveiller](https://www.goodmotion.fr/blog/les-elements-de-base-en-seo-a-mettre-en-place-sur-votre-site)
- [Ultime SEO meta tags](https://moz.com/blog/the-ultimate-guide-to-seo-meta-tags)
- [PWA with next](https://dev.to/anuraggharat/pwa-with-nextjs-5178)
- Resources :
  - https://betterprogramming.pub/boost-the-seo-of-your-websites-with-next-js-2ea29f5ae67
  - https://www.dbswebsite.com/blog/accessibility-seo-a-perfect-fit/
